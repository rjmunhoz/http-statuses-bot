import TelegramBot from 'tgfancy';
import dotenv from 'dotenv-safe';
dotenv.load();

const bot = new TelegramBot(process.env.TELEGRAM_TOKEN, { polling: true });
const baseUrl = 'https://httpstatuses.com/';

bot.getMe()
    .then(info => {
        console.log(`Deployed to ${info.username}`);
    });

bot.onText(/.*/, msg => {
    bot.sendMessage(msg.chat.id, 'Para me utilizar, digite meu nome de usuário no campo de texto', {
        reply_markup: {
            inline_keyboard: [[{ switch_inline_query: true }]]
        }
    });
});

const codes = [
    ['100', 'Continue'],
    ['101', 'Switching Protocols'],
    ['102', 'Processing'],
    ['200', 'OK'],
    ['201', 'Created'],
    ['202', 'Accepted'],
    ['203', 'Non-authoritative Information'],
    ['204', 'No Content'],
    ['205', 'Reset Content'],
    ['206', 'Partial Content'],
    ['207', 'Multi-Status'],
    ['208', 'Already Reported'],
    ['226', 'IM Used'],
    ['300', 'Multiple Choices'],
    ['301', 'Moved Permanently'],
    ['302', 'Found'],
    ['303', 'See Other'],
    ['304', 'Not Modified'],
    ['305', 'Use Proxy'],
    ['307', 'Temporary Redirect'],
    ['308', 'Permanent Redirect'],
    ['400', 'Bad Request'],
    ['401', 'Unauthorized'],
    ['402', 'Payment Required'],
    ['403', 'Forbidden'],
    ['404', 'Not Found'],
    ['405', 'Method Not Allowed'],
    ['406', 'Not Acceptable'],
    ['407', 'Proxy Authentication Required'],
    ['408', 'Request Timeout'],
    ['409', 'Conflict'],
    ['410', 'Gone'],
    ['411', 'Length Required'],
    ['412', 'Precondition Failed'],
    ['413', 'Payload Too Large'],
    ['414', 'Request-URI Too Long'],
    ['415', 'Unsupported Media Type'],
    ['416', 'Requested Range Not Satisfiable'],
    ['417', 'Expectation Failed'],
    ['418', 'I\'m a teapot'],
    ['421', 'Misdirected Request'],
    ['422', 'Unprocessable Entity'],
    ['423', 'Locked'],
    ['424', 'Failed Dependency'],
    ['426', 'Upgrade Required'],
    ['428', 'Precondition Required'],
    ['429', 'Too Many Requests'],
    ['431', 'Request Header Fields Too Large'],
    ['444', 'Connection Closed Without Response'],
    ['451', 'Unavailable For Legal Reasons'],
    ['499', 'Client Closed Request'],
    ['500', 'Internal Server Error'],
    ['501', 'Not Implemented'],
    ['502', 'Bad Gateway'],
    ['503', 'Service Unavailable'],
    ['504', 'Gateway Timeout'],
    ['505', 'HTTP Version Not Supported'],
    ['506', 'Variant Also Negotiates'],
    ['507', 'Insufficient Storage'],
    ['508', 'Loop Detected'],
    ['510', 'Not Extended'],
    ['511', 'Network Authentication Required'],
    ['599', 'Network Connect Timeout Error']
];

const getDescription = code => codes.filter(c => c[0] == code)[0][1];

const isCodeValid = code => (codes.filter(c => c[0] == code)).length > 0;

const giveExactCode = msg => {
    if (isCodeValid(msg.query)) {
        const code = msg.query;
        const response = {
            type: 'article',
            id: code,
            title: `${code} - ${getDescription(code)}`,
            input_message_content: {
                message_text: `[${code}](${baseUrl}${code})`,
                parse_mode: 'Markdown'
            }
        };
        bot.answerInlineQuery(msg.id, [response])
            .catch(console.error);
    }
};

const searchCode = msg => {
    const query = msg.query.toLowerCase();
    const matches = codes.filter(c => c[1].toLowerCase().indexOf(query) != -1);
    const results = matches.reduce((acc, el) => acc.add({
        type: 'article',
        id: el[0],
        title: `${el[0]} - ${el[1]}`,
        input_message_content: {
            message_text: `[${el[0]}](${baseUrl}${el[0]})`,
            parse_mode: 'Markdown'
        }
    }), new Set());

    if (results.size > 0)
        bot.answerInlineQuery(msg.id, Array.from(results))
            .catch(console.error);
};

bot.on('inline_query', msg => {
    const query = msg.query;
    if (query != '') {
        if (/[0-9]{1,3}/.test(query)) {
            giveExactCode(msg);
        } else {
            searchCode(msg);
        }
    }
});